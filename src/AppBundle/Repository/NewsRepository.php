<?php

namespace AppBundle\Repository;

/**
 * NewsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NewsRepository extends \Doctrine\ORM\EntityRepository
{
    public function getNewsLocale($locale, $limit)
    {
        return $this
            ->createQueryBuilder('news')
            ->where('news.language = :loc')
            ->setParameter('loc', $locale)
            ->orderBy('news.id', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findNewsData($data)
    {
        return $this
            ->createQueryBuilder('n')
            ->where('n.title = :data')
            ->orWhere('n.content = :data')
            ->setParameter('data', $data)
            ->getQuery()
            ->getResult();
    }


}
