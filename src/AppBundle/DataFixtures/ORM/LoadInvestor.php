<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Investor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadInvestor implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $investors = [
            [
                'name' => 'Тольбаев Бабур',
                'contribution' => '100000',
                'cash' => true,
                'description' => 'Вклад 100 тыс $'
            ],
            [
                'name' => 'Марат Джаанбаев',
                'contribution' => '150000',
                'cash' => false,
                'description' => 'Земля участок под ЦЛТ в дар (участок 70 соток)'
            ],
            [
                'name' => 'Коммерческий банк',
                'contribution' => '100000',
                'cash' => true,
                'description' => 'Вклад в 100 тыс $'
            ],
            [
                'name' => 'ОсОО "МКК Мол Булак"',
                'contribution' => '100000',
                'cash' => true,
                'description' => 'Вклад в 100 тыс $'
            ],
            [
                'name' => 'СК  "Мега Строй"',
                'contribution' => '100000',
                'cash' => false,
                'description' => 'Скидка 100 тыс $ на строительство здания ЦЛТ'
            ],
            [
                'name' => 'Абдулаев Жума Сагындыкович (ОсОО "Stocker")',
                'contribution' => '35000',
                'cash' => false,
                'description' => 'Лифт для ЦЛТ (35 000 $)'
            ],
            [
                'name' => 'Частный предприниматель Ж.Ж.',
                'contribution' => '50000',
                'cash' => false,
                'description' => 'Кирпичи на строительство здания ЦЛТ'
            ],
            [
                'name' => 'Улан Джумашев',
                'contribution' => '10000',
                'cash' => false,
                'description' => 'Комплекс офисной телефонии (в дар на 5 лет)'
            ],
            [
                'name' => 'Семья Бокошевых',
                'contribution' => '40000',
                'cash' => false,
                'description' => 'Недвижимость в качестве залога для кредита (40 000 $)'
            ],
        ];


        foreach ($investors as $value) {
            $investor = new Investor();
            $investor->setName($value['name']);
            $investor->setContribution($value['contribution']);
            $investor->setCash($value['cash']);
            $investor->setDescription($value['description']);
            $manager->persist($investor);
        }

        $manager->flush();
    }
}
