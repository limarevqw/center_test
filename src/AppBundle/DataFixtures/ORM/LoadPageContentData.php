<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Page;
use AppBundle\Entity\PageContent;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadPageContentData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $page_content1 = new PageContent();
        $page_content1->setTitle('Инфо о центре');
        $page_content1->setContent('Общественный фонд «АПАКЕ» и члены Наблюдательного Совета во главе с его Председателем Бабуром Тольбаевым презентуют "Центр Лучевой Терапии". Cоздание первого в истории Кыргызстана социального бизнеса в сфере здравоохранения. Этот проект ‑ беспрецедентный шаг для Кыргызстана в предоставлении доступного лечения для онкологических больных, благодаря синергии гражданского и бизнес сообщества.');
        $page_content1->setPublished(true);
        $page_content1->setPage('Главная');
        $manager->persist($page_content1);


        $page_content2 = new PageContent();
        $page_content2->setTitle('Инфо о центре');
        $page_content2->setContent('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, illo
                        voluptatibus!
                        Alias at dicta enim harum, ipsam neque obcaecati, possimus quas quisquam quod repellat
                        repudiandae sit. Officiis quos recusandae saepe. Lorem ipsum dolor sit amet, consectetur
                        adipisicing
                        elit. Assumenda, illo voluptatibus!
                        Alias at dicta enim harum, ipsam neque obcaecati, possimus quas quisquam quod repellat
                        repudiandae sit. Officiis quos recusandae saepe. Lorem ipsum dolor sit amet, consectetur
                        adipisicing
                        elit. Assumenda, illo voluptatibus!
                        Alias at dicta enim harum, ipsam neque obcaecati, possimus quas quisquam quod repellat
                        repudiandae sit. Officiis quos recusandae saepe.');
        $page_content2->setPublished(false);
        $page_content2->setPage('Главная');
        $manager->persist($page_content2);

        $manager->flush();
    }
}
