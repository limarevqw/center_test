<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Doctor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadSuperDoctor implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $super_doctor = new Doctor();

        $super_doctor
            ->setUsername('super1')
            ->setEmail('super1@super.ru')
            ->setRoles(['ROLE_SUPER_DOCTOR'])
            ->setEnabled(true)
            ->setName('Джумалиев Сергей Намырбекович')
            ->setBiography('Закончил Кыргызский Государственный Медицинский Институт 1979 г. 
                Специальность «Лечебное дело». С 1992г. ДиректорОсОО « Медицинского Центра Лазерных Технологий». 
                С 1986-1990г. СНС отдела «Опухоли брюшной полости» КНИИ Онкологии и радиологии. С 1983-1986г. 
                Аспирант Всесоюзного Онкологического Научного Центра АМН СССР. С 1981-1983г. МНС НИИ Онкологии 
                и радиологии отделения абдоминальной онкологии. С 1979-1981г. Клиническая ординатура в КНИИ 
                Онкологии и радиологии. Усовершенствования: 2012г. КГМИ Переподготовки и повышения квалификации 
                «Лазерная медицина». 2006г. Российская Мед. Академия г. Москва, «Лазерная медицина, лазерная 
                косметология и эстетическая хирургия». 1998г. Кыргызмедакадемия , «Врач-лазеролог». 1995г. 
                НИИ Лазерной Медицины г. Москва «Лазерная хирургия». 1991г. Киргизгосмединститут, 
                «Невропатология». 1991г.')
            ->setPosition('Главврач. Онколог, Хирург, Лазеролог')
            ->setPhoto('doctor11.jpg');
        $encoder = $this->container->get('security.password_encoder');
        $password1 = $encoder->encodePassword($super_doctor, '123');
        $super_doctor->setPassword($password1);

        $manager->persist($super_doctor);

        $manager->flush();
    }
}