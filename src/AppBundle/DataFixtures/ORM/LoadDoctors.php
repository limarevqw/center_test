<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Doctor;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadDoctors extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $doctors = [
            [
                'username' => 'doctor0',
                'email' => 'doctor0@ya.ru',
                'name' => 'Бейшенова Асель Рашидовна',
                'biography' => 'Кандидат медицинских наук. Отличник здравоохранения МЗ КР',
                'position' => 'Врач-онко-гинеколог',
                'photo' => 'doctor0.jpg'
            ],
            [
                'username' => 'doctor1',
                'email' => 'doctor1@ya.ru',
                'name' => 'Бегимкулова Айнура Сурановна',
                'biography' => 'Отличник здравоохранения МЗ КР',
                'position' => 'Врач-онколог',
                'photo' => 'doctor1.jpg'
            ],
            [
                'username' => 'doctor2',
                'email' => 'doctor2@ya.ru',
                'name' => 'Саргалдаков Зарлык Саргалдакович',
                'biography' => 'Национальный центр онкологии. Врач-онколог отделения абдоминальной и общей онкологи. 
                Отличник здравоохранения МЗ КР Ветеран труда Заслуженный врач КР',
                'position' => 'Врач онколог',
                'photo' => 'doctor2.jpg'
            ],
            [
                'username' => 'doctor3',
                'email' => 'doctor3@ya.ru',
                'name' => 'Джунушалиев Кубанычбек Кашымбекович',
                'biography' => 'Доктор медицинских наук. Отличник здравоохранения МЗ КР Почетная грамота 
                Мин.образования и науки КР',
                'position' => 'Зав.отделением опухолей головы, шеи',
                'photo' => 'doctor3.jpg'
            ],
            [
                'username' => 'doctor4',
                'email' => 'doctor4@ya.ru',
                'name' => 'Абакиров Эркин Абакирович',
                'biography' => 'Лечебно-оздоровительное объединение управления делами Президента КР (ЛОО УДП КР).
                Отличник здравоохранения КР',
                'position' => 'Врач высшей категори',
                'photo' => 'doctor4.jpg'
            ],
            [
                'username' => 'doctor5',
                'email' => 'doctor5@ya.ru',
                'name' => 'Лимарев Алексей Максимович',
                'biography' => 'Отличник здравоохранения МЗ КР Почетная грамота КР',
                'position' => 'Заведующий отделением предопухолевых заболеваний',
                'photo' => 'doctor5.jpg'
            ],
            [
                'username' => 'doctor6',
                'email' => 'doctor6@ya.ru',
                'name' => 'Сааданбекова Асиза Джумагуловна',
                'biography' => 'Врач онколог-гинеколог отделения онкогинекологии Отличник здравоохранения МЗ КР',
                'position' => 'Врач онколог-гинеколог',
                'photo' => 'doctor6.jpg'
            ]
        ];

        $doctor_patient1 = null;
        $doctor_patient2 = null;
        foreach ($doctors as $value)
        {
            $doctor = new Doctor();
            $doctor->setUsername($value['username']);
            $doctor->setEmail($value['email']);
            $doctor->setRoles(['ROLE_DOCTOR']);
            $doctor->setEnabled(true);
            $doctor->setName($value['name']);
            $doctor->setBiography($value['biography']);
            $doctor->setPosition($value['position']);
            $doctor->setPhoto($value['photo']);
            $encoder = $this->container->get('security.password_encoder');
            $password1 = $encoder->encodePassword($doctor, '123');
            $doctor->setPassword($password1);

            $manager->persist($doctor);
            if ($doctor->getUsername() == 'doctor0') {
                $doctor_patient1 = $doctor;
            } elseif ($doctor->getUsername() == 'doctor4') {
                $doctor_patient2 = $doctor;
            }
        }

        $manager->flush();

        $this->addReference('doctor0', $doctor_patient1);
        $this->addReference('doctor4', $doctor_patient2);
    }
}