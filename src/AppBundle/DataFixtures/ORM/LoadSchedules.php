<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Doctor;
use AppBundle\Entity\Patient;
use AppBundle\Entity\Schedule;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadSchedules implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $doctors = [];
        $doctor = new Doctor();
        $doctor->setUsername('Doctor22')
            ->setEmail('doctor22@mail.ru')
            ->setRoles(['ROLE_DOCTOR'])
            ->setEnabled(true)
            ->setName('Асылова Асель Тураровна')
            ->setBiography('Отличник здравоохранения МЗ КР Почетная грамота КР')
            ->setPosition('Врач онколог')
            ->setPhoto('doctor1.jpg');
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($doctor, '123');
        $doctor->setPassword($password);
        $manager->persist($doctor);
        $doctors[] = $doctor;

        $doctor2 = new Doctor();
        $doctor2->setUsername('Doctor23')
            ->setEmail('doctor23@mail.ru')
            ->setRoles(['ROLE_DOCTOR'])
            ->setEnabled(true)
            ->setName('Салиева Айнагуль Азимовна')
            ->setBiography('Отличник здравоохранения МЗ КР Почетная грамота КР')
            ->setPosition('Врач онколог')
            ->setPhoto('doctor1.jpg');
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($doctor2, '123');
        $doctor2->setPassword($password);
        $manager->persist($doctor2);
        $doctors[] = $doctor2;

        $patients_data = [
            [
                'name' => 'Иосиф',
                'surname' => 'Сталин',
                'patronymic' => 'Виссарионович',
                'inn' => '217091198601522',
                'birth_date' => new \DateTime()
            ],
            [
                'name' => 'Дмитрий',
                'surname' => 'Медведев',
                'patronymic' => 'Анатольевич',
                'inn' => '217091198601523',
                'birth_date' => new \DateTime()
            ]
        ];

        $patients = [];

        foreach ($patients_data as $value) {
            $patient = new Patient();
            $patient
                ->setName($value['name'])
                ->setSurname($value['surname'])
                ->setPatronymic($value['patronymic'])
                ->setInn($value['inn'])
                ->setBirthdate(new \DateTime());

            if ($value['name'] == 'Дмитрий') {
                $patient->setDoctor($doctor);
            } elseif ($value['name'] == 'Иосиф') {
                $patient->setDoctor($doctor2);
            }

            $manager->persist($patient);
            $patients[] = $patient;
        }

        foreach ($doctors as $doctor) {
            foreach ($patients as $patient) {
                for ($i = 1; $i < 6; $i++) {
                    $date = "2017-10-".$i;
                    for ($j = 13; $j < 18; $j++) {
                        if ($patient->getName() == 'Дмитрий' && $j%2 == 0) {
                            continue;
                        }
                        if ($patient->getName() == 'Иосиф' && $j%2 != 0) {
                            continue;
                        }
                        $time = $j.":00";
                        $type = null;
                        if (rand(1,2) == 1) {
                            $type = 'Консультация';
                        } else {
                            $type = 'Осмотр';
                        }
                        $schedule = new Schedule();
                        $schedule->setDoctor($doctor)
                            ->setPatient($patient)
                            ->setType($type)
                            ->setDate(\DateTime::createFromFormat('Y-m-d H:i', $date.' '.$time));
                        $manager->persist($schedule);
                    }

                }
            }
        }

        $manager->flush();
    }
}