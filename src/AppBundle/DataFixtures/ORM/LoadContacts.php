<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\PageContent;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadContacts implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $phone1 = new PageContent();
        $phone2 = new PageContent();

        $phone1->setTitle('Телефон');
        $phone1->setContent('<p>+996(555)555-555</p>');
        $phone1->setPublished(true);
        $phone1->setPage('Контакты');

        $phone2->setTitle('Телефон');
        $phone2->setContent('<p>+996(777)777-777</p>');
        $phone2->setPublished(true);
        $phone2->setPage('Контакты');

        $manager->persist($phone1);
        $manager->persist($phone2);
        $manager->flush();
    }
}
