<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Doctor;
use AppBundle\Entity\Patient;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadPatientsData extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $patients = [
            [
                'name' => 'Авраам',
                'surname' => 'Линкольн',
                'patronymic' => 'Линкольнович',
                'inn' => '217091198601551',
                'birth_date' => new \DateTime()
            ],
            [
                'name' => 'Джордж',
                'surname' => 'Вашингтон',
                'patronymic' => 'Джорджевич',
                'inn' => '217091198601552',
                'birth_date' => new \DateTime()
            ],
            [
                'name' => 'Бенджамин',
                'surname' => 'Франклин',
                'patronymic' => 'Франклинович',
                'inn' => '217091198601553',
                'birth_date' => new \DateTime()
            ],
            [
                'name' => 'Джордж',
                'surname' => 'Буш',
                'patronymic' => 'Старший',
                'inn' => '217091198601554',
                'birth_date' => new \DateTime()
            ],
            [
                'name' => 'Алмазбек',
                'surname' => 'Атамбаев',
                'patronymic' => 'Шаршенович',
                'inn' => '217091198601555',
                'birth_date' => new \DateTime()
            ],
            [
                'name' => 'Курманбек',
                'surname' => 'Бакиев',
                'patronymic' => 'Салиевич',
                'inn' => '217091198601556',
                'birth_date' => new \DateTime()
            ],
            [
                'name' => 'Аскар',
                'surname' => 'Акаев',
                'patronymic' => 'Акаевич',
                'inn' => '217091198601557',
                'birth_date' => new \DateTime()
            ],
        ];

        foreach ($patients as $value) {
            $patient = new Patient();
            $patient
                ->setName($value['name'])
                ->setSurname($value['surname'])
                ->setPatronymic($value['patronymic'])
                ->setInn($value['inn'])
                ->setBirthdate(new \DateTime());

            if ($value['name'] == 'Курманбек') {
                $patient->setDoctor($this->getReference('doctor0'));
            } elseif ($value['name'] == 'Аскар') {
                $patient->setDoctor($this->getReference('doctor4'));
            }

            $manager->persist($patient);
        }

        $manager->flush();
    }
}