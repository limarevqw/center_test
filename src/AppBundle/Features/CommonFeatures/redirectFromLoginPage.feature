# language: ru
@redirectFromLoginPage
Функционал: Тестируем редирект с маршрутка login если пользователь уже авторизован
  @loginUser @logout
  Сценарий: Выполнить проверку редиректа со страницы авторизации
    Допустим я перехожу на страницу "app_hospital_applications"
    И я перехожу на страницу "fos_user_security_login"
    Тогда меня редиректит на страницу "personal-area"