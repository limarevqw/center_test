<?php

namespace AppBundle\Features\Context;

use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines application features from the specific context.
 */
class UserLoginBeforeScenario extends RawMinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /** @BeforeScenario @loginUser*/
    public function LoginBeforeScenario()
    {
        $this->visitPath($this->getContainer()->get('router')->generate('fos_user_security_login'));
        sleep(2);
        $this->getSession()->getPage()->fillField('username', 'admin');
        $this->getSession()->getPage()->fillField('password', 'zxc123');
        $this->getSession()->getPage()->pressButton('_submit');
    }

    /** @BeforeScenario @loginDoctor*/
    public function doctorLoginBeforeScenario()
    {
        $this->visitPath($this->getContainer()->get('router')->generate('fos_user_security_login'));
        sleep(2);
        $this->getSession()->getPage()->fillField('username', 'doctor0');
        $this->getSession()->getPage()->fillField('password', '123');
        $this->getSession()->getPage()->pressButton('_submit');
    }
}