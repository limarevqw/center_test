<?php

namespace AppBundle\Features\Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use DateTime;
use PHPUnit_Framework_TestCase as PHPUnit;
use Behat\Mink\Exception\ExpectationException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }


    /**
     * @When /^я перехожу на страницу "([^"]*)"$/
     */
    public function яПерехожуНаСтраницу($route)
    {
        $this->visit($this->getContainer()->get('router')->generate($route));
        sleep(1);
    }

    /**
     * @When /^я заполняю поле: "([^"]*)" значением "([^"]*)" там$/
     */
    public function яЗаполняюПолеЗначениемТам($id, $date)
    {
        $this->getSession()->getPage()->find('css', '#' . $id)
            ->setValue($date);
    }


    /**
     * @When /^я авторизован как админ, нахожусь на своей персональной странице и перехожу по ссылке "([^"]*)"$/
     */
    public function яАвторизованКакАдминНахожусьНаСвоейПерсональнойСтраницеИПерехожуПоСсылке($arg1)
    {
        $this->clickLink($arg1);
        sleep(1);
    }


    /**
     * @When /^я перехожу по ссылке "([^"]*)"$/
     */
    public function яПерехожуПоСсылке($arg1)
    {
        $this->clickLink($arg1);
        sleep(1);
    }


    /**
     * @When /^заполняю поле "([^"]*)" сегодняшней датой$/
     */
    public function заполняюПолеСегодняшнейДатой($arg1)
    {
        $this->fillField($arg1, (new DateTime())->format('Y-m-d'));
        sleep(1);
    }

    /**
     * @When /^заполняю поле "([^"]*)" текущим временем$/
     */
    public function заполняюПолеТекущимВременем($arg1)
    {
        $this->fillField($arg1, (new DateTime())->format('H:i'));
        sleep(1);
    }

    /**
     * @When /^выбираю из списка следующее значение: "([^"]*)"$/
     */
    public function выбираюИзСпискаСледующееЗначение($arg1)
    {
        $this->selectOption('set-type', $arg1);
        sleep(1);
    }


    /**
     * @When /^меняю локаль на "([^"]*)" сайта$/
     */
    public function меняюЛокальНаСайта($loc)
    {
        $this->clickLink($loc);
        sleep(1);
    }


    /**
     * @When /^я нажимаю на ссылку "([^"]*)" по ИД\-селектору$/
     */
    public function яНажимаюНаСсылкуПоИДСелектору($id_link)
    {
        $this->getSession()->getPage()->find('css', $id_link)->click();
        sleep(1);
    }


    /**
     * @When /^я вижу слово "([^"]*)" где\\\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице1($string)
    {
        $this->assertPageContainsText($string);
    }


    /**
     * @When /^я открываю "([^"]*)"$/
     */
    public function яОткрываю($arg1)
    {
        $this->getSession()->getPage()->find('css', $arg1)->click();
    }

    /**
     * @When /^я нажимаю кнопку "([^"]*)"$/
     */
    public function яНажимаюКнопку($arg1)
    {
        $this->pressButton($arg1);
        sleep(1);
    }

    /**
     * @When /^я жду "([^"]*)" секунд$/
     */
    public function яЖдуСекунд($arg1)
    {
        sleep($arg1);
    }


    /**
     * @When /^перезагружаю страницу$/
     */
    public function перезагружаюСтраницу()
    {
        $this->getSession()->reload();
        sleep(2);
    }


    /**
     * @When /^я вижу блок с селектором "([^"]*)"$/
     */
    public function яВижуБлокССелектором($arg1)
    {
        $this->getSession()->getPage()->find('css', $arg1);
        sleep(2);
    }

    /**
     * @When /^меня редиректит на страницу "([^"]*)"$/
     */
    public function меняРедиректитНаСтраницу($arg1)
    {
        $this->assertPageAddress($arg1);
    }



    /**
     * @When /^я вижу появление элементов "([^"]*)"  в пустом блоке для Charts$/
     */
    public function яВижуПоявлениеЭлементовВПустомБлокеДляCharts($arg1)
    {
        $this->assertSession()->addressMatches('#/founder#');
        $this->getSession()->wait(2000, "$arg1");
        sleep(1);
    }



    /**
     * @When /^я вижу блок с классм "([^"]*)" постраничной навигацией$/
     */
    public function яВижуБлокСКлассмПостраничнойНавигацией($arg1)
    {
        $this->getSession()->getPage()->find('css', '.' . $arg1);
    }

    /**
     * @When /^вижу пункты меню:$/
     */
    public function вижуПунктыМеню(TableNode $table)
    {
        $hash = $table->getHash();
        foreach ($hash as $row) {
            $this->assertPageContainsText($row['menu']);
            sleep(1);
        }
    }


    /**
     * @When /^заполняю следующие поля значениеми:$/
     */
    public function заполняюСледующиеПоляЗначениеми(TableNode $table)
    {
        $hash = $table->getHash();
        foreach ($hash as $row) {
            $this->fillField($row['field'], $row['content']);
            sleep(1);
        }
    }

    /**
     * @When /^я авторизован и нахожусь на странице чата$/
     */
    public function яАвторизованИНахожусьНаСтраницеЧата()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_chat_index'));
        sleep(2);
    }

    /**
     * @When /^я ввожу сообщение "([^"]*)" в "([^"]*)" и нажимаю кнопку "([^"]*)"$/
     */
    public function яВвожуСообщениеВИНажимаюКнопку($arg1, $arg2, $arg3)
    {
        $this->fillField($arg2, $arg1);
        sleep(2);
        $this->pressButton($arg3);
        sleep(2);
    }

    /**
     * @When /^вижу появление сообщения "([^"]*)" блоке для чата "([^"]*)"$/
     */
    public function вижуПоявлениеСообщенияБлокеДляЧата($arg1, $arg2)
    {
        $this->assertElementContains('div', $arg1);
    }

}
