<?php

namespace AppBundle\Features\Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Mink\Exception\ExpectationException;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\KernelInterface;

class AdminFeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When /^я авторизован и нахожусь на странице администратора в разделе добавления новостей$/
     */
    public function яАвторизованИНахожусьНаСтраницеАдминистратораВРазделеДобавленияНовостей()
    {
        $this->visit($this->getContainer()->get('router')->generate('admin_app_news_create'));
        sleep(2);
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @When /^я заполняю поле заголовка$/
     */
    public function яЗаполняюПолеЗаголовка()
    {
        $this->fillField('Title', 'Тестовый заголовок');
    }

    /**
     * @When /^заполняю поле контента: "([^"]*)" значением "([^"]*)"$/
     */
    public function заполняюПолеКонтентаЗначением($arg1, $arg2)
    {
        $el = $this->getSession()->getPage()->findField($arg1);

        if (empty($el)) {
            throw new ExpectationException('Could not find WYSIWYG with locator: ' . $arg1, $this->getSession());
        }

        $fieldId = $el->getAttribute('id');

        if (empty($fieldId)) {
            throw new Exception('Could not find an id for field with locator: ' . $arg1);
        }

        $this->getSession()
            ->executeScript("CKEDITOR.instances[\"$fieldId\"].setData(\"$arg2\");");
    }

    /**
     * @When /^заполняю поле даты$/
     */
    public function заполняюПолеДаты()
    {
        $this->fillField('Date', '2017-07-07');
    }

    /**
     * @When /^нажимаю кнопку "([^"]*)"$/
     */
    public function нажимаюКнопку($arg1)
    {
        $this->pressButton('btn_create_and_edit');
        sleep(3);
    }

    /**
     * @When /^заполняю поле языка$/
     */
    public function заполняюПолеЯзыка()
    {
        $this->fillField('s2id_autogen1_search', 'Русский');
    }

    /**
     * @When /^я нахожусь в разделе добавления "([^"]*)" по пути "([^"]*)" на странице администратора$/
     */
    public function яНахожусьВРазделеДобавленияПоПутиНаСтраницеАдминистратора($investor, $route)
    {
        $this->visit($this->getContainer()->get('router')->generate($route));
        $this->assertPageContainsText($investor);
    }

    /**
     * @When /^я заполняю поле имени: "([^"]*)" значением "([^"]*)"$/
     */
    public function яЗаполняюПолеИмениЗначением($arg1, $arg2)
    {
        $this->fillField($arg1, $arg2);
        sleep(2);
    }

    /**
     * @When /^заполняю поле биографии: "([^"]*)" значением "([^"]*)"$/
     */
    public function заполняюПолеБиографииЗначением($arg1, $arg2)
    {
        $this->fillField($arg1, $arg2);
        sleep(2);
    }

    /**
     * @When /^нажимаю на кнопку "([^"]*)"$/
     */
    public function нажимаюНаКнопку($arg1)
    {
        $this->pressButton('btn_create_and_edit');
        sleep(1);
    }

    /**
     * @When /^загружаю картинку в поле: "([^"]*)"$/
     */
    public function загружаюКартинкуВПоле($arg1)
    {
        $this->attachFileToField($arg1, '/home/salamkg/Рабочий стол/urolog1.jpg');
        sleep(5);
    }


    /**
     * @When /^заполняю поле: "([^"]*)" значением "([^"]*)"$/
     */
    public function заполняюПолеЗначением($field, $value)
    {
        $this->fillField($field, $value);
    }

    /**
     * @When /^ставлю галочку на чекбоксе по ИД "([^"]*)" на странице$/
     */
    public function ставлюГалочкуНаЧекбоксеПоИДНаСтранице($id_label)
    {
        $this->getSession()->getPage()->find('css', $id_label)->click();
    }
}