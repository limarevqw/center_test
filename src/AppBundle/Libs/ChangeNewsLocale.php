<?php
/**
 * Created by PhpStorm.
 * User: tuk
 * Date: 27.10.17
 * Time: 16:25
 */

namespace AppBundle\Libs;

use Doctrine\ORM\EntityManager;


class ChangeNewsLocale
{
    public $em;

    public function __construct(EntityManager $em = null)
    {
        $this->em = $em;
    }

    public function changeNewsLocale($locale, $limit)
    {
        $news = null;

        if ($locale == 'ru') {
            $news = $this->em->getRepository('AppBundle:News')
                ->getNewsLocale('Русский', $limit);
        } else {
            $news = $this->em->getRepository('AppBundle:News')
                ->getNewsLocale('Кыргызский', $limit);
        }
        return $news;
    }

}