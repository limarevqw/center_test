<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChatController extends Controller
{
    /**
     * @Route("/chat")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        return $this->render('AppBundle:Chat:chat.html.twig', array(
            'user' => $user
        ));
    }

    /**
     * @Route("/save-message")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function saveMessageAction(Request $request)
    {
        $data = json_decode($request->request->get('data'));

        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->find($data->id);

        $message = new Message();
        $message->setUser($user);
        $message->setMessage($data->message);
        $message->setDatetime(new \DateTime());

        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        $response_data = [
            'username' => $user->getUsername()
        ];

        return new Response(json_encode($response_data), JSON_UNESCAPED_UNICODE);
    }

    /**
     * @Route("/load-chat")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loadChatAction(Request $request)
    {
        $messages = [];
        $raw_messages = $this->getDoctrine()
            ->getRepository('AppBundle:Message')
            ->getMessages();

        foreach ($raw_messages as $raw_message) {
            $messages[] = [
                'message' => $raw_message['message'],
                'id' => $raw_message['id'],
                'datetime' => $raw_message['datetime']->format('Y-m-d H:i'),
                'username' => $raw_message['username']
            ];
        }

        $messages = array_reverse($messages);

        return new Response(json_encode($messages), JSON_UNESCAPED_UNICODE);
    }
}