<?php

namespace AppBundle\Controller;

use AppBundle\Libs\ChangeNewsLocale;
use AppBundle\Entity\Applications;
use AppBundle\Entity\AppResponse;
use AppBundle\Entity\News;
use AppBundle\Form\ReceptionRegistrationType;
use AppBundle\Form\SendEmailType;
use AppBundle\Libs\GetRandomDoctor;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig_Environment;

class MainController extends Controller
{
    /**
     * @Route("/")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        if ($request->getSession()->get('_locale') == null) {
            $request->getSession()->set('_locale', 'ru');
        }


        $news_locale = new ChangeNewsLocale($this->getDoctrine()->getManager());
        $news = $news_locale->changeNewsLocale($request->getSession()->get('_locale'), 3);
        
        $random_doctor = new GetRandomDoctor($this->getDoctrine()->getManager());
        $doctor = $random_doctor->getRandomDoctor();

        $form = $this->createForm(ReceptionRegistrationType::class);

        $sliders = $this->getDoctrine()
            ->getRepository('AppBundle:Slider')
            ->findSlidersPublished();

        $contents = $this->getDoctrine()->getRepository('AppBundle:PageContent')
            ->findBy(['page' => 'Главная', 'published' => true]);

        return $this->render('AppBundle:Main:index.html.twig', array(
            'news' => $news,
            'doctor' => $doctor,
            'form_popup' => $form->createView(),
            'sliders' => $sliders,
            'contents' => $contents
        ));
    }

    public function getPhoneContentAction()
    {
        $widget = $this->get('my_widget');
        $phones = $widget->getPhones();

        return $this->render('AppBundle:Main:header.html.twig', array(
            'widget' => $widget,
        ));
    }

    /**
     * @Route("/investor")
     */
    public function investorAction()
    {
        $investors = $this->getDoctrine()
            ->getRepository('AppBundle:Investor')
            ->findAll();
        return $this->render('AppBundle:Main:investor.html.twig', array(
            'investors' => $investors
        ));
    }

    /**
     * @Route("/founder")
     */
    public function founderAction()
    {

        $investors = $this->getDoctrine()
            ->getRepository('AppBundle:Investor')
            ->findAllSortMoreContribution();

        return $this->render('AppBundle:Main:founder.html.twig', array(
            'investors' => $investors
        ));
    }

    /**
     * @Route("/investor/json")
     */
    public function investorJsonAction()
    {

        $investors_cash_true = $this->getDoctrine()
            ->getRepository('AppBundle:Investor')
            ->findBy(['cash' => true]);

        $investors_cash_false = $this->getDoctrine()
            ->getRepository('AppBundle:Investor')
            ->findBy(['cash' => false]);

        $name_investors_cash_false = '';
        $contribution_investors_cash_false = [];

        foreach ($investors_cash_false as $not_cash) {
            $contribution_investors_cash_false[] = $not_cash->getContribution();
            $name_investors_cash_false .= $not_cash->getName() . ', ' . '<br>';
        }

        $temp_array = [];
        $names = [];
        $moneys = [];
        $data_array = [];
        $i = 0;
        foreach ($investors_cash_true as $investor) {
            $names[$i] = $investor->getName();
            $moneys[$i] = $investor->getContribution();//->getMoney();
            $i++;
        }
        for ($j = 0; $j < count($names); $j++) {
            $temp_array[0] = $names[$j];
            $temp_array[1] = $moneys[$j];
            $data_array[$j] = $temp_array;
        }

        $data_array[] = [$name_investors_cash_false, array_sum($contribution_investors_cash_false)];

        return new JsonResponse($data_array);
    }

    /**
     * @Route("/{_locale}", requirements = {"_locale" : "ru|kg"}, name="changeLocal")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocal(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/doctors")
     */
    public function doctorsAction()
    {
        $doctors = $this->getDoctrine()
            ->getRepository('AppBundle:Doctor')
            ->findAll();

        return $this->render('@App/Main/doctors.html.twig', array(
            'doctors' => $doctors
        ));
    }

    /**
     * @Route("/doctor/{id}", requirements={"id": "\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function doctorProfileAction(int $id)
    {
        $doctor = $this->getDoctrine()
            ->getRepository('AppBundle:Doctor')
            ->find($id);

        return $this->render('@App/Main/doctor_profile.html.twig', array(
            'doctor' => $doctor
        ));
    }

    /**
     * @Route("/news")
     * @param Request $request
     * @return Response
     * @internal param Request $request
     */
    public function moreNewsAction(Request $request)
    {
        $news_locale = new ChangeNewsLocale($this->getDoctrine()->getManager());
        $news = $news_locale->changeNewsLocale($request->getSession()->get('_locale'), null);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('@App/Main/news.html.twig', array(
            'news' => $news,
            'pagination' => $pagination
        ));
    }

    /**
     * @Route("/news/all")
     * @param Request $request
     * @return Response
     * @internal param Request $request
     */
    public function allNewsAction(Request $request)
    {
        $news = $this->getDoctrine()->getRepository(News::class)->findAll();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news,
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('@App/Main/all_news.html.twig', array(
            'news' => $news,
            'pagination' => $pagination
        ));
    }


    /**
     *
     * @Route("news/{id}", requirements={"id": "\d+"})
     * @Method({"GET", "POST"})
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showOneNews(int $id)
    {
        $one_news = $this->getDoctrine()
            ->getRepository('AppBundle:News')
            ->find($id);

        return $this->render('AppBundle:Main:one_news.html.twig', [
            'one_news' => $one_news,
        ]);
    }


    /**
     * @Route("/save_application")
     * @param Request $request
     * @Method("POST")
     * @return Response
     */
    public function saveApplication(Request $request)
    {
        $application = new Applications();

        $form = $this->createForm(ReceptionRegistrationType::class, $application);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $application->setExamined(false);
            $application->setDate(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($application);
            $em->flush();

            return new Response(json_encode(array('status'=>'success')));
        }

        return new Response(json_encode(array('status'=>'fail')));
    }

    /**
     * @Route("/save_email/{application_id}")
     * @param Request $request
     * @param $application_id
     * @return Response
     * @Method("POST")
     */
    public function saveEmail(Request $request, $application_id)
    {
        $text_body = $request->request->get('app_bundle_send_email_type');

        $application = $this->getDoctrine()
            ->getRepository(Applications::class)
            ->find($application_id);
        $email = $request->request->get('email');
        $form = $this->createForm(SendEmailType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $response = new AppResponse();
            $response->setUser($this->getUser());
            $response->setMessage($text_body['record']);
            $response->setDate(new \DateTime());
            $response->setApplication($application);
            $em->persist($response);
            $em->flush();

            $message = \Swift_Message::newInstance()
                ->setSubject('Центр Лучевой Терапии')
                ->setFrom('test.luch.center@gmail.com')
                ->setTo($email)
                ->setBody($text_body['record']);

            $this->get('mailer')->send($message);

            return new Response(json_encode(array('status'=>'success')));
        }

        return new Response(json_encode(array('status'=>'fail')));
    }

    /**
     * @Route("/contacts")
     * @Method("GET")
     */
    public function contactsAction()
    {
        return $this->render('AppBundle:Main:contacts.html.twig');
    }

    /**
     * @Route("/partners")
     * @Method("GET")
     */
    public function partnersAction()
    {
        return $this->render('AppBundle:Main:partners.html.twig');
    }

    /**
     * @Route("/equipment")
     * @Method("GET")
     */
    public function equipmentAction()
    {
        return $this->render('AppBundle:Main:equipment.html.twig');
    }

    /**
     * @Route("/initiators")
     * @Method("GET")
     */
    public function initiatorsAction()
    {
        return $this->render('AppBundle:Main:initiators.html.twig');
    }
}
