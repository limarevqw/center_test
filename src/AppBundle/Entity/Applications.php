<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Applications
 *
 * @ORM\Table(name="applications")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicationsRepository")
 */
class Applications
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="examined", type="boolean")
     */
    private $examined;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AppResponse", mappedBy="application")
     */
    private $app_repsonces;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Applications
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Applications
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Applications
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Applications
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set examined
     *
     * @param boolean $examined
     *
     * @return Applications
     */
    public function setExamined($examined)
    {
        $this->examined = $examined;

        return $this;
    }

    /**
     * Get examined
     *
     * @return bool
     */
    public function getExamined()
    {
        return $this->examined;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Applications
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->app_repsonces = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add appRepsonce
     *
     * @param \AppBundle\Entity\AppResponse $appRepsonce
     *
     * @return Applications
     */
    public function addAppRepsonce(\AppBundle\Entity\AppResponse $appRepsonce)
    {
        $this->app_repsonces[] = $appRepsonce;

        return $this;
    }

    /**
     * Remove appRepsonce
     *
     * @param \AppBundle\Entity\AppResponse $appRepsonce
     */
    public function removeAppRepsonce(\AppBundle\Entity\AppResponse $appRepsonce)
    {
        $this->app_repsonces->removeElement($appRepsonce);
    }

    /**
     * Get appRepsonces
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppRepsonces()
    {
        return $this->app_repsonces;
    }
}
