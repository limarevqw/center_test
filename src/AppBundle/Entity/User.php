<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({"fos_user" = "User", "doctor" = "Doctor"})
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AppResponse", mappedBy="user")
     */
    private $app_repsonces;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Message", mappedBy="user")
     */
    private $messages;

    public function __construct()
    {
        parent::__construct();
        $this->messages = new ArrayCollection();
    }

    /**
     * Add appRepsonce
     *
     * @param \AppBundle\Entity\AppResponse $appRepsonce
     *
     * @return User
     */
    public function addAppRepsonce(\AppBundle\Entity\AppResponse $appRepsonce)
    {
        $this->app_repsonces[] = $appRepsonce;

        return $this;
    }

    /**
     * Remove appRepsonce
     *
     * @param \AppBundle\Entity\AppResponse $appRepsonce
     */
    public function removeAppRepsonce(\AppBundle\Entity\AppResponse $appRepsonce)
    {
        $this->app_repsonces->removeElement($appRepsonce);
    }

    /**
     * Get appRepsonces
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAppRepsonces()
    {
        return $this->app_repsonces;
    }

    /**
     * Add message
     *
     * @param \AppBundle\Entity\Message $message
     *
     * @return User
     */
    public function addMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\Message $message
     */
    public function removeMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
