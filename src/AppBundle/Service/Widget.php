<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Widget
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getPhones()
    {
        return $this->container
            ->get('doctrine')
            ->getRepository('AppBundle:PageContent')
            ->findBy(['page' => 'Контакты', 'published' => true]);
    }
}