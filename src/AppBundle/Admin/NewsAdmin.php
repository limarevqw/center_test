<?php

namespace AppBundle\Admin;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class NewsAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title', null, array('label' => 'Заголовок'))
            ->add('date', null, array('label' => 'Дата'))
            ->add('language', null, array('label' => 'Язык'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title', null, array('label' => 'Заголовок'))
            ->add('date', null, array('label' => 'Дата'))
            ->add('language', null, array('label' => 'Язык'))
            ->add('_action', null, array(
                'label' => 'Действия',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Новости')
            ->add('title', TextType::class, [
                'label' => 'Заголовок'
            ])
            ->add('content', CKEditorType::class, array(
                'label' => 'Текст',
                'config'      => array('toolbar' => 'basic'),
            ))
            ->add('date', 'sonata_type_date_picker', array(
                'label' => 'Дата',
                'data' => new \DateTime()
            ))
            ->add('language', ChoiceType::class, array('choices' => array(
                'Русский' => 'Русский',
                'Кыргызский' => 'Кыргызский'),
                'label' => 'Язык'
            ))
            ->add('imageFile', FileType::class, array(
                'label' => false,
                'required' => false
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Новости')
            ->add('title', null, array('label' => 'Заголовок'))
            ->add('content', null, array('label' => 'Текст'))
            ->add('date', null, array('label' => 'Дата'))
            ->add('language', null, array('label' => 'Язык'))
            ->add('news_image', null, array('label' => 'Изображение'))
        ;
    }
}
