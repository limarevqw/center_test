<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class InvestorAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array('label' => 'Инвестор'))
            ->add('contribution', null, array('label' => 'Вклад'))
            ->add('description', null, array('label' => 'Описание'))
            ->add('cash', null, array('label' => 'В денежном ввиде'));
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, array('label' => 'Инвестор'))
            ->add('contribution', null, array('label' => 'Вклад'))
            ->add('description', null, array('label' => 'Описание'))
            ->add('cash', null, array('label' => 'Наличными'))
            ->add('_action', null, array(
                'label' => 'Действия',
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Инвестор')
            ->add('name', TextType::class, [
                'label' => 'Инвестор'
            ])
            ->add('contribution', MoneyType::class, [
                'label' => 'Вклад'
            ])
            ->add('cash', CheckboxType::class, [
                'label' => 'В денежном ввиде',
                'label_attr' => ['id' => 'id_cash'],
                'required' => false,
            ])

            ->add('description', TextareaType::class, [
                'label' => 'Описание'
            ]);
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Инвестор')
            ->add('name', null, array('label' => 'Инвестор'))
            ->add('contribution', null, array('label' => 'Вклад'))
            ->add('description', null, array('label' => 'Описание'))
            ->add('cash', null, array('label' => 'В денежном ввиде'));
    }
}
