<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;


class LoginRegisterListener
{
    /** @var  ContainerInterface */
    private $container;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @param ContainerInterface $container
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $tokenStorage
     */
    public function __construct(ContainerInterface $container, TokenStorage $tokenStorage)
    {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $is_logged_in = $this->getUser() instanceof User;
        $has_user_role = $is_logged_in && in_array('ROLE_USER', $this->getUser()->getRoles());
        $is_on_login_page = $event->getRequest()->get('_route') == 'fos_user_security_login';

        if ($has_user_role && $is_on_login_page) {
            $url = $this->container
                ->get('router')
                ->generate('app_hospital_personalarea');

            $event->setResponse(new RedirectResponse($url));
        }
    }
}